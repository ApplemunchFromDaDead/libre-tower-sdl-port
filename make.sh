# !/bin/sh
# Make script for Wake to Hell, includes frontends and packaging
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (sdl, etc)"
	exit 1
fi

if [ $1 = "sdl" ]; then
	$COMPILER main.c -lSDL2 -lSDL2_mixer -lSDL2_image $2 -o LibreTower
elif [ $1 = "package" ]; then
	BUILDDIR="BUILD/"

	cp WakeToHell ${BUILDDIR}
	cp *.mid ${BUILDDIR}
	cp *.mod ${BUILDDIR}
	cp -r sfx ${BUILDDIR}
	cp -r sprites ${BUILDDIR}
	cp DOCS/LICENSES.txt ${BUILDDIR}
	cp DOCS?LICENSE_CC0.txt ${BUILDDIR}
	echo "The game (assuming your binary is named WakeToHell) and all required files for a *NIX build are now in the $BUILDDIR folder."
elif [ $1 = "love" ]; then # lol
	echo "No the fuck you will not."
fi
