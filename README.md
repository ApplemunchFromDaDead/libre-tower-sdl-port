# Libre Tower SDL Port

Libre Tower is a FOSS fangame of Pizza Tower that's more oriented on slower, thoughtful gameplay, while still retaining what makes Pizza Tower relatively fun.

This is a port of the game from GMS2 to SDL2, since the former is not only proprietary, but also unable to compile for Windows and Linux for free.

All game code is under CC0; previously it was BSD 3-Clause, but I, blitzdoughnuts / Applemunch Games, choose to relicense my code into the public domain. This also applies to any and all game assets originally made by me.
