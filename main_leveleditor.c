// for ease of compilation, I merged the level editor's code into 1 file
// game functions are STILL SEPARATED, the lvl editor is just one file now

// SDL2 frontend for the game

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#undef main

#define GAME_H

uint16_t cursor_pos[2];

typedef struct {

} LTE_Editor

void start() {

};

void step() {

};

#endif

SDL_Surface *winsurf;
SDL_Texture *wintext;

SDL_Texture *sprites[6];

SDL_Rect dsp_rect; // this is used for the sprite's boundaries
SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out
SDL_Rect rect_draw; // used for drawing rectangles on screen

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

FILE *DaSave;

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < WIDTH && limit <= 30; limit++) {
		if (_x < (-WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	_x = x - dspdest_rect.w;
	dspdest_rect.x = _x;
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawColBox(WTH_ColBox boxin) {
	SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
	int16_t in[2];
    in[0] = boxin.x - cam.x;
    in[1] = boxin.y - cam.y;
    rect_draw.x = in[0];
    rect_draw.y = in[1];
    rect_draw.w = boxin.xscale;
    rect_draw.h = boxin.yscale;
    SDL_RenderDrawRect(render, &rect_draw);
};

void drawRect(int x, int y, int w, int h) {
	SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
    rect_draw.x = x;
    rect_draw.y = y;
    rect_draw.w = w;
    rect_draw.h = h;
    SDL_RenderDrawRect(render, &rect_draw);
};

// inline the simple stuff
static inline void signalPlaySFX(uint8_t signal) {
	Mix_PlayChannel(-1, sfx[signal], 0);
};

static inline void signalPlayMUS(uint8_t signal) {
	//if (options[0] & WTHOPTS_DOSONG) Mix_PlayMusic(music[signal], -1);
	Mix_PlayMusic(music[signal], -1);
};	// ditto, but for music

void draw() {
	drawSpriteSheeted(plrsprites[0], plr.x - cam.x, plr.y - cam.y, plr.image_index, 100, 100, (plr.flags & FLAG_FLIPPED) ? 1 : 0);
	drawColBox(plr.colbox);
    for (uint8_t i = 0; i < 255; i++) {
        if (!solids[i].flags) continue;
        drawColBox(solids[i].colbox);
    };
    
    for (uint8_t i = 0; i < 255; i++) {
        if (!(interacts[i].flags & INTER_ACTIVE)) continue;
        switch (interacts[i].objID)
        {
        	case INTERTYPE_COLLECT:
        		drawRect(interacts[i].x, interacts[i].y, interacts[i].xscale, interacts[i].yscale);
				drawSpriteSheeted(sprites[1 + interacts[i].vars[0]], interacts[i].x - cam.x, interacts[i].y - cam.y, 0, 32, 32, 0);
        		break;
        }
    };

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
};

uint8_t *keystates;

void keys() {
    #if LT_SDL_DEMOSUPPORT == 1
    if (demoStatus == 3) return;
    #endif
	if (keystates[SDL_SCANCODE_LEFT]) input_keys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_RIGHT]) input_keys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_UP]) input_keys |= KEY_UP;
	if (keystates[SDL_SCANCODE_DOWN]) input_keys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_Z]) input_keys |= KEY_JUMP;
	if (keystates[SDL_SCANCODE_X]) input_keys |= KEY_ATTACK;
	if (keystates[SDL_SCANCODE_LSHIFT]) input_keys |= KEY_DASH;
	if (keystates[SDL_SCANCODE_ESCAPE]) input_keys |= KEY_MENU;
	
	if (keystates[SDL_SCANCODE_A]) xtrakeys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_D]) xtrakeys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_W]) xtrakeys |= KEY_UP;
	if (keystates[SDL_SCANCODE_S]) xtrakeys |= KEY_DOWN;
};

int main() {
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
 MIX_INIT_MID) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The editor couldn't find a usable audio system. Check your audio devices!", NULL);
	
	#define REALWINTITLE WINTITLE " " VERSION_NUMBER
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, 960, 540); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	winsurf = SDL_GetWindowSurface(win);

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	sprites[0] = IMG_LoadTexture(render, "sprites/bg.png");
	sprites[1] = IMG_LoadTexture(render, "sprites/collectibles_1.png");
	sprites[2] = IMG_LoadTexture(render, "sprites/collectibles_2.png");
	sprites[3] = IMG_LoadTexture(render, "sprites/collectibles_3.png");
	sprites[4] = IMG_LoadTexture(render, "sprites/collectibles_4.png");
	sprites[5] = IMG_LoadTexture(render, "sprites/plr_idle.png");
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		keys();
		draw();
		step();
		
		//wintext = SDL_CreateTextureFromSurface(render, winsurf);
		//SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		//SDL_DestroyTexture(wintext);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
