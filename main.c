// SDL2 frontend for the game

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

#define LT_SDL_DEMOSUPPORT 1

// strings here, for both ease of access and also to reduce redundancy in program string lists
static const char str_savegamename[] = "LT_SaveGame.bin";

Mix_Music *music[1];

Mix_Chunk *sfx[3];

SDL_Surface *winsurf;
SDL_Texture *wintext;

SDL_Texture *plrsprites[5];
SDL_Texture *sprites[5];

SDL_Rect dsp_rect; // this is used for the sprite's boundaries
SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out
SDL_Rect rect_draw; // used for drawing rectangles on screen

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

FILE *DaSave;

#if LT_SDL_DEMOSUPPORT == 1
FILE *DaDemo;
uint8_t demoStatus;
uint8_t demowrite[1];
uint32_t demoPointer;
#endif

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < WIDTH && limit <= 30; limit++) {
		if (_x < (-WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	_x = x - dspdest_rect.w;
	dspdest_rect.x = _x;
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawColBox(WTH_ColBox boxin) {
	SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
	int16_t in[2];
    in[0] = boxin.x - cam.x;
    in[1] = boxin.y - cam.y;
    rect_draw.x = in[0];
    rect_draw.y = in[1];
    rect_draw.w = boxin.xscale;
    rect_draw.h = boxin.yscale;
    SDL_RenderDrawRect(render, &rect_draw);
};

void drawRect(int x, int y, int w, int h) {
	SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
    rect_draw.x = x;
    rect_draw.y = y;
    rect_draw.w = w;
    rect_draw.h = h;
    SDL_RenderDrawRect(render, &rect_draw);
};

void signalMisc(uint8_t signal) {
    switch (signal)
    {
        case MISC_PAUSEMUSIC: case MISC_RESUMEMUSIC: case MISC_STOPMUSIC:
            if (!(options[0] & WTHOPTS_DOSONG)) break;
            switch (signal)
            {
                case MISC_PAUSEMUSIC: Mix_PauseMusic(); break;
                case MISC_RESUMEMUSIC: Mix_ResumeMusic(); break;
                case MISC_STOPMUSIC: Mix_HaltMusic(); break;
            }
            break;
    }
};

void signalDraw(uint8_t index) {
	switch (index)
	{
		case DRAW_FADEIN: case DRAW_FADEOUT: case DRAW_HALTFADE:
			if (options[0] & WTHOPTS_NOFADE) break;
			switch (index)
			{
				case DRAW_FADEIN:
					fade = 255;
					fademode = 1;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_FADEOUT:
					fade = 0;
					fademode = 2;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_HALTFADE:
					fade = 0;
					fademode = 0;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
					break;
			};
			break;
		case DRAW_PLRUPARROW:
            drawSprite(sprites[7], plr.x - 20 - cam.x, plr.y - 150 - cam.y);
            break;
		case DRAW_CHECK2XSIZE:
			if (options[0] & WTHOPTS_2XSIZE)
				SDL_SetWindowSize(win, 1920, 1080);
			else
				SDL_SetWindowSize(win, 960, 540);
			SDL_SetWindowPosition(win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
			break;
		case DRAW_CHECKFULLSC:
			if (options[0] & WTHOPTS_FULLSC)
				SDL_SetWindowFullscreen(win, SDL_WINDOW_FULLSCREEN_DESKTOP);
			else
				SDL_SetWindowFullscreen(win, 0);
			break;
	};
};

// inline the simple stuff
static inline void signalPlaySFX(uint8_t signal) {
	Mix_PlayChannel(-1, sfx[signal], 0);
};

static inline void signalPlayMUS(uint8_t signal) {
	//if (options[0] & WTHOPTS_DOSONG) Mix_PlayMusic(music[signal], -1);
	Mix_PlayMusic(music[signal], -1);
};	// ditto, but for music

void saveGame() {
	DaSave = fopen(str_savegamename,"w");
	fwrite(&options, 1, 2, DaSave);
	fclose(DaSave);
};

void loadGame() {
	if (!(DaSave = fopen(str_savegamename,"r"))) return;
	fread(&options, 1, 1, DaSave);
	fclose(DaSave);
};

void draw() {
	drawRepeatingSprite(sprites[0], 0 - (cam.x >> 1), 0 - (cam.y >> 1));
	drawSpriteSheeted(plrsprites[plr.sprite_index], plr.x - cam.x, plr.y - cam.y, plr.image_index, 100, 100, (plr.flags & FLAG_FLIPPED) ? 1 : 0);
	drawColBox(plr.colbox);
    for (uint8_t i = 0; i < 255; i++) {
        if (!solids[i].flags) continue;
        drawColBox(solids[i].colbox);
    };
    
    for (uint8_t i = 0; i < 255; i++) {
        if (!(interacts[i].flags & INTER_ACTIVE)) continue;
        switch (interacts[i].objID)
        {
        	case INTERTYPE_COLLECT:
        		drawRect(interacts[i].x - cam.x, interacts[i].y - cam.y, interacts[i].xscale, interacts[i].yscale);
				drawSpriteSheeted(sprites[1 + interacts[i].vars[0]], interacts[i].x - cam.x, interacts[i].y - cam.y, interacts[i].vars[1], 32, 32, 0);
        		break;
        }
    };

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
};

uint8_t *keystates;

void keys() {
    #if LT_SDL_DEMOSUPPORT == 1
    if (demoStatus == 3) return;
    #endif
	if (keystates[SDL_SCANCODE_LEFT]) input_keys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_RIGHT]) input_keys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_UP]) input_keys |= KEY_UP;
	if (keystates[SDL_SCANCODE_DOWN]) input_keys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_Z]) input_keys |= KEY_JUMP;
	if (keystates[SDL_SCANCODE_X]) input_keys |= KEY_ATTACK;
	if (keystates[SDL_SCANCODE_LSHIFT]) input_keys |= KEY_DASH;
	if (keystates[SDL_SCANCODE_ESCAPE]) input_keys |= KEY_MENU;
	
	if (keystates[SDL_SCANCODE_A]) xtrakeys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_D]) xtrakeys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_W]) xtrakeys |= KEY_UP;
	if (keystates[SDL_SCANCODE_S]) xtrakeys |= KEY_DOWN;
};

int main() {
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
 MIX_INIT_MID) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);
	
	#define REALWINTITLE WINTITLE " " VERSION_NUMBER
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, 960, 540); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	winsurf = SDL_GetWindowSurface(win);
	
	//music[0] = Mix_LoadMUS("WTH.s3m"); 
	music[0] = Mix_LoadMUS("D_AGM.mid"); 

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	sfx[0] = Mix_LoadWAV("sfx/step.wav");
	sfx[1] = Mix_LoadWAV("sfx/jump.wav");
	sfx[2] = Mix_LoadWAV("sfx/slam.wav");
	
	plrsprites[spr_player_idle] = IMG_LoadTexture(render, "sprites/plr_idle.png");
	plrsprites[spr_player_move] = IMG_LoadTexture(render, "sprites/plr_walk.png");
	sprites[0] = IMG_LoadTexture(render, "sprites/bg.png");
	sprites[1] = IMG_LoadTexture(render, "sprites/collectibles_1.png");
	sprites[2] = IMG_LoadTexture(render, "sprites/collectibles_2.png");
	sprites[3] = IMG_LoadTexture(render, "sprites/collectibles_3.png");
	sprites[4] = IMG_LoadTexture(render, "sprites/collectibles_4.png");
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		keys();
        #if LT_SDL_DEMOSUPPORT == 1
        if (keystates[SDL_SCANCODE_F1] && demoStatus != 2) {
            if (demoStatus != 1) {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Libre Tower", "Recording demo...", NULL);
                DaDemo = fopen("LT_Demo.wthdem", "w");
                demoStatus = 1;
            } else {
                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Libre Tower", "Demo recording stopped", NULL);
                fclose(DaDemo);
                demoStatus = 0;
            };
        }
        if (keystates[SDL_SCANCODE_F2] && demoStatus != 1) {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Libre Tower", "Playing back demo...", NULL);
            DaDemo = fopen("LT_Demo.wthdem", "r");
            demoStatus = 2;
        }
        switch (demoStatus & 0b00000011)
        {
            case 1:
                demowrite[0] = input_keys;
                fwrite(demowrite, 1, 1, DaDemo);
                break;
            case 2:
                fread(demowrite, 1, 1, DaDemo);
                input_keys = demowrite[0];
               // printf("%i", demowrite[0]);
                fseek(DaDemo, demoPointer, SEEK_SET);
                demoPointer++;
                break;
        }
        #endif
		if (fade < 255) draw();
		step();
		
		//wintext = SDL_CreateTextureFromSurface(render, winsurf);
		//SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		//SDL_DestroyTexture(wintext);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
