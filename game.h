/*
GAME.H
This file defines SPECIFICALLY the game logic, completely separate from any libraries.
Drawing && handling of sprites happens in the main_[platform].c file.
YES, this is from Wake to Hell. Bite me.
*/

// these define the amount of frames in animations
#define PLR_IDLEFRAMES 10 // no anims were ported yet lmao

#define INTERTYPE_COLLECT 0
#define INTERTYPE_DESTROYABLE 1
#define INTERTYPE_TRANSITION 2
#define INTERTYPE_DESTDOOR 255 // used for marking where the player could transition to when going between rooms

// according to Suckless, floats are overrated lmao
#define SUBPIXELUNIT_ACCURACY 128
// floats can be done, it just needs adaptation

// PRECALCULATIONS!!!
#define PRE_GRAVITY SUBPIXELUNIT_ACCURACY >> 2 // precalculated gravity, also doubles as player movement speed
#define PLR_WALKSPEED 102 // SUBPIXELUNIT_ACCURACY * 0.8
#define PLR_MAXSPEED 7
#define PRE_CAMBOUNDS 128

#define FLAG_GROUNDED 1
#define FLAG_CANTMOVE 1 << 1 // disables player control, if any
#define FLAG_NOPHYS 1 << 2 // universal; if set, object refuses to move at all
#define FLAG_FLIPPED 1 << 3
#define FLAG_STEPDEBOUNCE 1 << 4 // used to see if the player already did a footstep

// ======== SOLID FLAGS
#define SFLAG_ACTIVE 1 << 7 // same as INTER_ACTIVE
#define SFLAG_SLOPED 1 // overridden by next
#define SFLAG_PLATFM 1 << 1 // overrides slope flag

#define PSTATE_NORMAL 0
#define PSTATE_RUN 1
#define PSTATE_GRAB 2
#define PSTATE_HURT 3
#define PSTATE_STUN 4
#define PSTATE_SJUMP 5

// player sprite names, to fill in gaps GMS2 left

#define spr_player_idle 0
#define spr_player_move 1
#define spr_player_jump 2
#define spr_player_fall 3
#define spr_player_dash 4

// interactible flags
#define INTER_ACTIVE 1 << 7 // thing flag; is this thing NOT "in the room"?

#define KEY_UP 1
#define KEY_DOWN 1 << 1
#define KEY_LEFT 1 << 2
#define KEY_RIGHT 1 << 3
#define KEY_JUMP 1 << 4
#define KEY_ATTACK 1 << 5
#define KEY_MENU 1 << 6
#define KEY_DASH 1 << 7

// game states
#define GSTATE_MENU 0
#define GSTATE_PLAYING 1
#define GSTATE_BLANKSCREEN 2
#define GSTATE_PAUSED 3

#define GROUNDLEVEL 200

#define WIDTH 960
#define HEIGHT 540

#define WTHOPTS_NOFADE 1 // disables fading in/out for performance reasons
#define WTHOPTS_DEVCAM 1 << 1 // a toggle for the free camera
#define WTHOPTS_DOSONG 1 << 2 // play music?
#define WTHOPTS_2XSIZE 1 << 6 // 2x the res?
#define WTHOPTS_FULLSC 1 << 7 // fullscreen enabled?

// draw calls
#define DRAW_PLRUPARROW 1

#define DRAW_FADEIN 16 // begin fading in from black
#define DRAW_FADEOUT 17 // begin fading into black
#define DRAW_HALTFADE 18 // clear the screen of fading entirely

#define DRAW_CHECK2XSIZE 64
#define DRAW_CHECKFULLSC 65

#define DRAW_UPDATEMENU 127

#define MISC_PAUSEMUSIC 0
#define MISC_RESUMEMUSIC 1
#define MISC_STOPMUSIC 2

#define INTER_LIMIT 256 // how many interactibles can be on a level at a time?

#define WINTITLE "Libre Tower"

#define VERSION_NUMBER "SDL_B1pa" // SDL Build 1, Pre-Alpha

#define WTH_keyPressed(keyvalue) (input_keys & keyvalue) && !(input_prevkeys & keyvalue)
// easy way to check if a key was down but NOT held, saves space for comment rambling

void signalDraw(uint8_t signal); // interface with main for drawing specific things at specific times
static inline void signalPlaySFX(uint8_t signal); // same stuff, but for sound effects
static inline void signalPlayMUS(uint8_t signal); // ditto, but for music
void signalMisc(uint8_t signal); // ditto, but for anything else

uint8_t GAME_STATE = 0;
uint16_t POINTS = 0;

uint8_t game_rng, cosmetic_rng;

static uint8_t game_getrandom() {
	game_rng *= 13;
	game_rng += 7;
	return game_rng;
};

static uint8_t cosmetic_getrandom() {
	cosmetic_rng *= 13;
	cosmetic_rng += 7;
	return cosmetic_rng;
};

typedef struct {
    int x, y, xscale, yscale;
} WTH_ColBox;

typedef struct {
	int x, y, hsp, vsp;
	int hsp_sub, vsp_sub;
	uint8_t flags;
	uint8_t state;
	uint8_t image_index, animtimer, image_count, sprite_index;
	WTH_ColBox colbox;
	int16_t statevars[4];
} WTH_Player;

typedef struct {
	uint8_t menuselect;
	uint8_t menuindex:4,
			menulimit:4;
} WTH_MainMenu;
// menuindex && menulimit are 4-bit variables (up to 15)

typedef struct {
	int x, y;
} WTH_Camera;

typedef struct {
	int x, y, xscale, yscale;
	uint16_t vars[8];
	uint8_t objID, flags;
} WTH_Interactible;

typedef struct {
    int x, y;
    WTH_ColBox colbox;
    uint8_t flags;
} WTH_Solid;

WTH_Player plr; // the one && only... uh... unstable furry man.
WTH_Camera cam; // && also the cameraman
WTH_Interactible interacts[INTER_LIMIT];
WTH_Solid solids[256];
uint8_t interacts_count = 0; // convenient counter for all present interactibles

uint8_t level; // all rooms, 256 is overkill but overkill is what's needed
uint8_t level_nospawn[255][INTER_LIMIT]; // a list of objects to NOT spawn based on their spawn order
uint8_t fade, fademode; // the variable managing screen fade
// remember that FADE dictates ALPHA, NOT the RGB values
uint8_t fadespeed = 3;

WTH_MainMenu menu; // main menu

uint8_t options[2];

uint8_t input_keys;
uint8_t input_prevkeys;
uint8_t xtrakeys;

void saveGame();
void loadGame();
// frontend, please add details

uint8_t addObject(int nx, int ny, uint8_t objType) {
	if (interacts_count >= INTER_LIMIT) {
		printf("addObject: Tried adding object over the limit!");
		return 0;
	};
	uint8_t index = interacts_count;
	for (uint8_t i = 0; i < 7; i++) {
		interacts[index].vars[i] = 0;
	};
	interacts[index].x = nx;
	interacts[index].y = ny;
	interacts[index].objID = objType;
	interacts[index].flags |= INTER_ACTIVE;
	interacts_count++;
	
	switch (objType)
	{
		case INTERTYPE_COLLECT:
			interacts[index].xscale = interacts[index].yscale = 32;
			interacts[index].vars[0] = game_getrandom() % 4; // assign random sprite
			interacts[index].vars[1] = game_getrandom() % 3; // assign random offset for animation
			break;
	}
	
	/// printf("\nCreated object number %i at (%i, %i) of object type %i", index, interacts[index].x, interacts[index].y, interacts[index].objID);
	return index;
};

#define makeSolid(id, _x, _y, _xscale, _yscale) solids[id].flags = SFLAG_ACTIVE; \
	solids[id].x = solids[id].colbox.x = _x; \
	solids[id].y = solids[id].colbox.y = _y; \
	solids[id].colbox.xscale = _xscale * 32; \
	solids[id].colbox.yscale = _yscale * 32

void loadLevelFromFile(char *pathtolvl);

void loadLocalLevel(uint8_t lvl) {
	switch (lvl)
	{
		case 0:
			plr.x = 448;
			plr.y = 350;
			plr.colbox.x = plr.x;
			plr.colbox.y = plr.y;
			plr.colbox.xscale = 38;
			plr.colbox.yscale = 83;
			makeSolid(0, 0, 448, 60, 3);
			makeSolid(1, 0, 192, 2, 8);
			makeSolid(2, 0, 0, 8, 3);
			makeSolid(3, 704, 0, 38, 3);
			makeSolid(4, 1024, 224, 17, 4);
			makeSolid(5, 728, 224, 6, 4);
			makeSolid(6, 64, 256, 5, 3);
			addObject(576, 384, INTERTYPE_COLLECT);
			addObject(618, 384, INTERTYPE_COLLECT);
			addObject(640, 384, INTERTYPE_COLLECT);
			addObject(672, 384, INTERTYPE_COLLECT);
			addObject(1584, 160, INTERTYPE_COLLECT);
			addObject(1616, 160, INTERTYPE_COLLECT);
			addObject(1648, 160, INTERTYPE_COLLECT);
			addObject(1680, 160, INTERTYPE_COLLECT);
			addObject(1584, 384, INTERTYPE_COLLECT);
			addObject(1616, 384, INTERTYPE_COLLECT);
			addObject(1648, 394, INTERTYPE_COLLECT);
			addObject(1680, 384, INTERTYPE_COLLECT);
			addObject(64, 384, INTERTYPE_COLLECT);
			addObject(96, 418, INTERTYPE_COLLECT);
			addObject(96, 352, INTERTYPE_COLLECT);
			addObject(128, 384, INTERTYPE_COLLECT);
			break;
	}
}

uint8_t checkCollision(WTH_ColBox *box1, WTH_ColBox *box2, int box1_offsetX, int box1_offsetY, int box2_offsetX, int box2_offsetY) {
    return (!(
        box2->x + box2_offsetX > box1->x + box1_offsetX + box1->xscale ||
        box2->x + box2_offsetX + box2->xscale < box1->x + box1_offsetX ||
        box2->y + box2_offsetY > box1->y + box1_offsetY + box1->yscale ||
        box2->y + box2_offsetY + box2->yscale < box1->y + box1_offsetY
    ));
};

uint8_t checkCollision_Instance(WTH_ColBox *box, WTH_Interactible *inter, int box_offsetX, int box_offsetY, int inter_offsetX, int inter_offsetY) {
    return (!(
        inter->x + inter_offsetX > box->x + box_offsetX + box->xscale ||
        inter->x + inter_offsetX + inter->xscale < box->x + box_offsetX ||
        inter->y + inter_offsetY > box->y + box_offsetY + box->yscale ||
        inter->y + inter_offsetY + inter->yscale < box->y + box_offsetY
    ));
};

uint8_t checkPointCollision(int px, int py, WTH_ColBox *box) {
    return (!(
        px > box->x + box->xscale ||
        py > box->y + box->yscale
    ));
};

void interact_step(WTH_Interactible *REF) {
	if (!(REF->flags & INTER_ACTIVE)) return;
	switch (REF->objID)
	{
		case INTERTYPE_COLLECT:
			REF->vars[2]++;
			if (REF->vars[2] > 2) {
				REF->vars[1]++;
				REF->vars[2] = 0;
				if (REF->vars[1] > 2) REF->vars[1] = 0;
			};
			if (checkCollision_Instance(&plr.colbox, REF, 0,0,0,0)) {
				REF->flags ^= INTER_ACTIVE;
				POINTS += 10;
				// play collect sound
			};
			break;
	}
};

void manageOptions() {
	switch (menu.menuselect)
	{
		case 0: case 1: options[0] ^= 1 << menu.menuselect; break; // trust me, this shit works
		case 2: options[0] ^= WTHOPTS_2XSIZE; signalDraw(DRAW_CHECK2XSIZE); break;
		case 3: options[0] ^= WTHOPTS_FULLSC; signalDraw(DRAW_CHECKFULLSC); break;
		case 4: options[0] ^= WTHOPTS_DOSONG; break;
		case 5: saveGame(); menu.menuindex = menu.menuselect = 0; menu.menulimit = 2; break;
	}
};

void plr_checkCollide() {
	if (plr.hsp) {
		for (uint8_t i = 0; i < 255; i++) {
		    if (!(solids[i].flags & SFLAG_ACTIVE)) continue;
		    //printf("X Collision: Looping solid collision...\n");
		    if (checkCollision(&plr.colbox, &solids[i].colbox, plr.hsp, 0, 0, 0)) {
		        //printf("X Collision: Got a solid!\n");
		        int8_t plrdir = (plr.hsp < 0) ? -1 : 1;
		        while (!checkCollision(&plr.colbox, &solids[i].colbox, plrdir, 0, 0, 0)) {
		            plr.x += plrdir;
		            plr.colbox.x = plr.x + 31;
		        }
		        plr.hsp = 0;
		    };
		};
    };
    plr.x += plr.hsp;
    plr.colbox.x = plr.x + 31;

	if (!(plr.flags & FLAG_GROUNDED)) plr.vsp_sub += SUBPIXELUNIT_ACCURACY >> 1;
	for (uint8_t i = 0; i < 255; i++) {
	    if (!(solids[i].flags & SFLAG_ACTIVE)) continue;
	   // printf("Y Collision: Looping solid collision...\n");
	    if (checkCollision(&plr.colbox, &solids[i].colbox, 0, plr.vsp, 0, 0)) {
	        //printf("Y Collision: Got a solid!\n");
	        int8_t plrdir = (plr.vsp < 0) ? -1 : 1;
	        while (!checkCollision(&plr.colbox, &solids[i].colbox, 0, plrdir, 0, 0)) {
	            plr.y += plrdir;
	            plr.colbox.y = plr.y + 14;
	        }
	        plr.vsp = 0;
	    };
		if (checkPointCollision(plr.x + 50, plr.y + 102, &solids[i].colbox)) {
		    if (!(plr.flags & FLAG_GROUNDED)) plr.flags ^= FLAG_GROUNDED;
		} else {
		    if (plr.flags & FLAG_GROUNDED) plr.flags ^= FLAG_GROUNDED;
		};
	};
    plr.y += plr.vsp;
    plr.colbox.y = plr.y + 14;
};

int clamp(int16_t val, int16_t min, int16_t max) {
	if (val < min) return min;
	if (val > max) return max;
	return val;
};

void plr_step() {
	// to-do: turn this file https://github.com/Applemunch/LibreTower/blob/main/scripts/scr_plrscripts/scr_plrscripts.gml into C states
	switch (plr.state)
	{
		case PSTATE_NORMAL: default:
			if (!(plr.flags & FLAG_GROUNDED)) {
				plr.sprite_index = spr_player_fall;
			}
			if (plr.flags & FLAG_CANTMOVE) return;
			if ((input_keys & KEY_UP) && (plr.flags & FLAG_GROUNDED)) {
				plr.hsp = 0;
				//sprite_index = spr_player_hjump_prep
			} else {
				if ((input_keys & KEY_LEFT) || (input_keys & KEY_RIGHT)) {
					if (!((input_keys & KEY_LEFT) && (input_keys & KEY_RIGHT))) {
						plr.hsp_sub += (input_keys & KEY_LEFT) ? -PLR_WALKSPEED : PLR_WALKSPEED;
						plr.hsp = clamp(plr.hsp, -PLR_MAXSPEED, PLR_MAXSPEED);
						if ((plr.flags & FLAG_FLIPPED) && plr.hsp > 0) plr.hsp = 0;
						if (!(plr.flags & FLAG_FLIPPED) && plr.hsp < 0) plr.hsp = 0;
						plr.sprite_index = spr_player_move;
						plr.image_count = 3;
						if ((input_keys & KEY_LEFT) && !(plr.flags & FLAG_FLIPPED)) {
							plr.flags ^= FLAG_FLIPPED;
						} else if ((input_keys & KEY_RIGHT) && (plr.flags & FLAG_FLIPPED)) {
							plr.flags ^= FLAG_FLIPPED;
						};
					}
				} else {
					plr.hsp = plr.hsp_sub = 0;
					plr.sprite_index = spr_player_idle;
					plr.image_count = PLR_IDLEFRAMES;
				}
			}
			if ((input_keys & KEY_JUMP) && (plr.flags & FLAG_GROUNDED)) {
				plr.vsp = -6;
				signalPlaySFX(1);
			}
			if (input_keys & KEY_ATTACK) {
				plr.state = PSTATE_GRAB;
				plr.statevars[0] = 60;
			};
			if (input_keys & KEY_DASH) {
				plr.state = PSTATE_RUN;
			};
			if (!(input_keys & KEY_DOWN)) {
				for (uint8_t i = 0; i < 255; i++) {
					if (!(solids[i].flags & SFLAG_ACTIVE)) continue;
					if (checkCollision(&plr.colbox, &solids[i].colbox, 0, -36, 0, 0)) {
						return;
					};
				};
			}; // don't crouch
			//crouched = scr_buttoncheck(vk_down, gp_padd)
			printf("%i\n", plr.sprite_index);
			break;
		case PSTATE_HURT:
			break;
		case PSTATE_STUN:
			break;
		case PSTATE_GRAB:
			plr.sprite_index = spr_player_dash;
			plr.hsp = (plr.flags & FLAG_FLIPPED) ? -8 : 8;
			for (uint8_t i = 0; i < 255; i++) {
				if (!(solids[i].flags & SFLAG_ACTIVE)) continue;
				if (checkCollision(&plr.colbox, &solids[i].colbox, plr.hsp, 0, 0, 0)) {
					plr.state = PSTATE_NORMAL;
					return;
				};
			};
			/*
			COLLIDE WITH AN INSTANCE
			if it's destroyable, destroy it
			if colliding into something, then what the fuck are you doing?
			*/
			//for (uint8_t i = 0; i < 255; i++) {
			//	if (!(instances[i].flags & INTER_ACTIVE)) continue;
			//};
			/*
			COLLIDE WITH AN EXIT SWITCH
			if you do, turn on panic
			if panic is already on or the switch is already dusted, do nothing
			*/
			plr.statevars[0] -= 1;
			if (plr.statevars[0] <= 0) {
				plr.state = PSTATE_NORMAL;
			}
			break;
		case PSTATE_RUN:
			//plr.sprite_index = spr_player_run;
			if (plr.hsp < 12 && !(plr.flags & FLAG_FLIPPED)) plr.hsp_sub += 40;
			if (plr.hsp > -12 && (plr.flags & FLAG_FLIPPED)) plr.hsp_sub -= 40;
			if (!(input_keys & KEY_DASH)) plr.state = PSTATE_NORMAL;
			break;
		//case (PSTATE_runturn):
			//break;
		case PSTATE_SJUMP:
			break;
	};
	printf("%i\n", plr.hsp_sub);
};

void start() {
	plr.colbox.x = plr.x;
	plr.colbox.y = plr.y;
	plr.colbox.xscale = 38;
	plr.colbox.yscale = 83;
	plr.sprite_index = 0;
	plr.image_count = PLR_IDLEFRAMES;
	plr.state = PSTATE_NORMAL;
	menu.menuselect = 0;
	loadLocalLevel(0);
	signalPlayMUS(0);
};

void step() {
    if (plr.image_index >= plr.image_count) plr.image_index = 0;
    plr.animtimer++;
    if (plr.animtimer > 4) {
        plr.animtimer = 0;
        plr.image_index++;
    };
	plr_step();
	
	if (plr.vsp_sub > SUBPIXELUNIT_ACCURACY) {
	    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
		plr.vsp += calc;
		plr.vsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
	};
	if (plr.vsp_sub < -SUBPIXELUNIT_ACCURACY) {
	    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
		plr.vsp -= calc;
		plr.vsp_sub += SUBPIXELUNIT_ACCURACY * calc;
	};
	
	if (plr.hsp_sub > SUBPIXELUNIT_ACCURACY) {
	    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
		plr.hsp += calc;
		plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
	};
	if (plr.hsp_sub < -SUBPIXELUNIT_ACCURACY) {
	    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
		plr.hsp += calc;
		plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
	};
	
	plr_checkCollide();
	
	for (uint16_t i = 0; i < INTER_LIMIT; i++) {
		interact_step(&interacts[i]);
	};

	if (plr.x > cam.x + (WIDTH - PRE_CAMBOUNDS)) {
		cam.x += plr.x - (cam.x + (WIDTH - PRE_CAMBOUNDS));
	};
	if (plr.x < cam.x + PRE_CAMBOUNDS) {
		cam.x -= (cam.x + PRE_CAMBOUNDS) - plr.x;
	};
	if (plr.y > cam.y + (HEIGHT - PRE_CAMBOUNDS)) {
		cam.y += plr.y - (cam.y + (HEIGHT - PRE_CAMBOUNDS));
	};
	if (plr.y < cam.y + PRE_CAMBOUNDS) {
		cam.y -= (cam.y + PRE_CAMBOUNDS) - plr.y;
	};
	
	input_keys = 0;
};
